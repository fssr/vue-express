const express = require('express')
const router = express.Router()

const moment = require('moment')

const model = require('../config/db.js')
const checkToken = require('../middleware/checkToken.js')
const objectIdToTimestamp = require('objectid-to-timestamp')

const jsonWrite = (res, ret) => {
  if (typeof ret === 'undefined') {
    res.json({
      code: '404',
      msg: 'server is error'
    })
  } else {
    res.json(ret)
  }
}

router.post('/useradd', (req, res, next) => {
  const params = req.body

  const name = params.name
  const email = params.email
  const pwd = params.pwd
  
  const user = new model.User({
    name: name,
    email: email,
    pwd: pwd
  })
  user.create_time = moment(objectIdToTimestamp(user._id))
    .format('YYYY-MM-DD HH:mm:ss')

  let regName = /^[a-zA-Z ]{2,30}$/  
  let regEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/
  let regPwd = /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,20}$/

  if (regName.test(name) && regEmail.test(email) && regPwd.test(pwd)) {
    model.User.findOne({
      name: user.name
    }, (err, doc) => {
      if (err) console.log(err)
      if (doc) {
        const result = {
          code: 50,
          msg: 'Error! Existing user',
          tip: ''
        }
        jsonWrite(res, result)
      } else {
        user.save(err => {
          if (err) {
            const result = {
              code: 100,
              msg: 'Error! Error saving user details',
              tip: 'Please try again'
            }
            jsonWrite(res, result)
          } else {
            const result = {
              code: 200,
              msg: 'User details are saved',
              tip: 'Thank you for sharing the details'
            }
            jsonWrite(res, result)
          }
        })
      }
    })
  } else {
    const result = {
      code: 100,
      msg: 'Error! Error saving user details',
      tip: 'Please try again'
    }
    jsonWrite(res, result)
  }
})

router.get('/getuser', checkToken, (req, res, next) => {
  model.User.find({}, (err, doc) => {
    if (err) console.log(err)
    jsonWrite(res, doc)
  })
})

module.exports = router

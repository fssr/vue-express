import Vue from 'vue'
import Router from 'vue-router'
import Register from '@/pages/user/Register'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: {
        name: 'reg'
      }
    },
    {
      path: '/reg',
      name: 'reg',
      component: Register
    }
  ]
})

export default router

# vue-express-mongodb

> Vue.js and Express and MongoDB project

# Technology

- [x] vue
- [x] vue-router
- [x] vuex
- [x] express
- [x] mongodb
- [x] less/scss
- [x] axios
- [x] ElementUI

# API

- `/api/user/useradd`
- `/api/user/login`
- `/api/user/getuser`

# Schema

```javascript
const userSchema = mongoose.Schema({
  userId: {
    type: Number,
    required: true
  },
  userPwd: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  create_time: {
    type: Date,
    required: true
  }
})
```

# Build Setup

```bash
# install dependencies
npm install

# run express server，this should be the first execution
# the server api run on localhost:3000
npm run server

# serve with hot reload at localhost:8080
# [HPM] Proxy created: /api  ->  http://127.0.0.1:3000/api/
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```